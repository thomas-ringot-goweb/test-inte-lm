$(document).ready(function() {
  var $sticky = $('.sticky-menu');
  var $stickylimit = $('.garden')

  if (!!$sticky.offset()) {

    var stickyHeight = $sticky.innerHeight();
    var stickyOffsetTop = $sticky.offset().top;
    var stickyOffset = 20;
    var stickyStopperPosition = $stickylimit.offset().top;
    var stopStickyPoint = stickyStopperPosition - stickyHeight - stickyOffset;
    var stickyDifference = stopStickyPoint + stickyOffset;

    $(window).scroll(function(){
      var windowTop = $(window).scrollTop();
      if (stopStickyPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: stickyDifference });
      } else if (stickyOffsetTop < windowTop+stickyOffset) {
          $sticky.css({ position: 'fixed', top: stickyOffset });
      } else {
          $sticky.css({position: 'absolute', top: '40%'});
      }
    });
  }
});
