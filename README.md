# TEST D'INTEGRATION LEROY MERLIN

### SETUP

```
git clone https://bitbucket.org/thomas-ringot-goweb/test-inte-lm/src/master/
cd test-inte-lm
npm install
```

### SCRIPTS NPM

```
#dev
npm run dev
#build
npm run build
```
